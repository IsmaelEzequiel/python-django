#-*- Encoding: utf-8 -*-
#Implementar uma função que receba um dicionário e retorne a soma, a
#média e a variação dos valores.

def func(x, y):
	soma = x + y
	media = (x + y) / 2
	return soma, media
	#sum(dic.values()) // Values < Pega todos os valores do dicionario

def me(dic):
	a = sum(dic.values())
	b = a * len(dic)
	c = max(dic.values()) - min(dic.values())
	return a, b, c
a = {
	"valor1" : 10,
	"valor2" : 20,
	"valor3" : 20
}

print func(a['valor1'], a['valor2'])
print me(a)
