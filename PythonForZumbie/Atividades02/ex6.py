def mut(a, b='ezequiel', *c, **d):
    """Padrao doc string

    esse e o padrao para se usar doc string, muito interessante.
    """
    print (("%s %s") % (a, b))
    for i in c:
        print (i)
    print ("-" * 40)
    for k in d.keys():
        print (k, ":",  d[k])

d = {
    'a' : 'ismael',
    'b' : 'Troool',
    'c' : ['TUUUPLAS', 'MAIS', 'TUPLAS'],
    'd' : ['DDDD', 'kel']
}
print (mut.__doc__)
print (mut(**d))
