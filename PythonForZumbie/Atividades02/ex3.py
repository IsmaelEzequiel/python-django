#-*- Encoding: utf-8 -*-
#Sorteie 20 inteiros entre 1 e 100 numa lista. Armazene os números pares na lista PAR e os
#números ímpares na lista IMPAR. Imprima as três listas.

import random
par = []
impar = []
for a in range(20):
	a = random.randint(1,100)
	if a % 2 == 0:
		par.append(a)
	else:
		impar.append(a)

print "Par: " , par
print "Impar: ", impar
		
