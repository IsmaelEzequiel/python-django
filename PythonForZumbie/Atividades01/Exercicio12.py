#-*- Encoding: utf-8 -*-

#Sorteie 20 inteiros entre 1 e 100 numa lista. Armazene os números pares na lista PAR e os
#números ímpares na lista IMPAR. Imprima as três listas.
import random

par = []
impar = []
lista = []
for i in range(20):
	lista.append(random.randint(1, 100))
	if lista[i]  % 2 == 0:
		s = str(lista[i])
		print "par: ", ''.join(s)
	else:
		c = str(lista[i])	
		print "impar: ", ''.join(c)
